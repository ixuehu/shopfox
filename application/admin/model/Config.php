<?php
namespace app\admin\model;

use think\Model;

class Config extends Model
{
    public function saveConfig($data){
        if(empty($data) || !is_array($data)){
            return false;
        }
        foreach ($data as $key => $value) {
            if(empty($key)){
                //结束本次循环
                continue;
            }
            //更新
            Config::where('name', $key)->update(['value'=>trim($value)]);
        }
        return true;
    }
}
