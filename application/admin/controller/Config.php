<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use app\admin\model\Config as ConfigModel;

class Config extends Common
{
    public function index(){
        $configList = ConfigModel::column('name,value');
        $this->assign('config', $configList);
        return view();
    }

    public function edit(){
        if(request()->isPost()){
            $config = new ConfigModel;
            if($config->saveConfig(input('post.'))){
                return success('配置更新成功', url('index'));
            }else{
                return error('配置更新失败', url('index'));
            }
        }
    }
}
