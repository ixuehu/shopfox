<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Validate;

class Coupon extends Common
{
  public function index($id = 0, $tab = 1){
      if(3 == $tab){
          $info = Db::name('coupon')->where('id', $id)->find();
          if($info){
              $this->assign('info', $info);
          }
      }

      return view();
  }

  public function add(){
      //先判断post提交
      if(request()->isPost()){
          $data = input('post.');

          // 验证器
          $validate = new Validate([
              'name' =>'require|max:200'
          ]);
          if(!$validate->check($data)){
              return error('请输入优惠券名称');
          }

          $data['start_time'] = strtotime($data['start_time']);
          $data['end_time'] = strtotime($data['end_time']) + 86399;
          $result = Db::name('coupon')->strict(false)->insert($data); //strict为false不进行字段严格检查
          if($result){
              return success('添加成功',url('index'));
          }else{
              return error('添加失败');
          }
      }
  }

  public function edit(){
      if(request()->isPost()){
          $data = input('post.');
          $result = Db::name('ad')->strict(false)->update($data);
          //如果信息未改变提交更新会返回false
          if($result !== false){
              return success('更新成功',url('index'));
          }else{
              return error('更新失败');
          }
      }
  }

  //删除功能
  public function delete($id=0){
      if(Db::name('coupon')->where('id', $id)->setField('is_delete', 1)){
          return success('删除成功',url('index'));
      }else{
          return error('已经失效或已被删除');
      }
  }

  public function getDataTables() {
      //获取请求过来的数据
      $getParam = request()->param();

      $draw = $getParam['draw'];

      //排序
      $orderSql = $getParam['order'][0]['dir'];

      //自定义查询参数
      $extra_search = $getParam['extra_search'];

      //获取表名
      $tablename = request()->controller();
      // 总记录数
      $recordsTotal = Db::name($tablename)->count();
      //过滤条件后的总记录数
      $search = $getParam['search']['value'];
      $recordsFiltered = strlen($search) ?  Db::name($tablename)->where($extra_search,'like','%'.$search.'%')->count() : $recordsTotal;

      //分页
      $start = $getParam['start']; //起始下标
      $length = $getParam['length']; //每页显示记录数

      //根据开始下标计算出当前页
      $page = intval($start/$length) + 1;
      $config = ['page'=>$page, 'list_rows'=>$length];
      $list = Db::name($tablename)->where($extra_search,'like','%'.$search.'%')->order($orderSql)->paginate(null,false,$config);
      $lists = [];
      if(!empty($list)){
          foreach ($list as $key => $value) {
              $lists[$key] = $value;
              // 优惠方式 discount_type
              switch ($value['type']) {
                  case 0:
                      $lists[$key]['type'] = '满减券';
                      $lists[$key]['discount_type'] = '立减 ' .$value['price']. ' 元';
                      break;
                  case 1:
                      $lists[$key]['type'] = '折扣券';
                      $lists[$key]['discount_type'] = '打 ' . ($value['discount']/10) . ' 折';
                      break;
                  default:
                      break;
              }

              // 有效期 expiry_date
              $lists[$key]['expiry_date'] = date('Y-m-d', $value['start_time']) . ' ～ ' . date('Y-m-d', $value['end_time']);

              $order_status = $lists[$key]['order_status'];
              $lists[$key]['order_status'] = "<span id='order_status_".$value['id']."'>".$order_status."</span>";

              // 判断优惠券有效期或已经软件删除
              if($value['end_time'] < time() || $value['is_delete']){
                  $lists[$key]['name'] = "<div style='color:#999;text-decoration:line-through'>" . $lists[$key]['name'] . "</div>";
                  $lists[$key]['expiry_date'] = "<div style='color:#999;text-decoration:line-through'>" . $lists[$key]['expiry_date'] . "</div>";
                  $lists[$key]['operate'] = "";
              }else{
                  $lists[$key]['operate'] = "<a id='".$value['id']."' receive_num='".$value['receive_num']."' href='javascript:;' title='编辑' class='coupon_edit'><i class='fa fa-edit text-navy'></i></a>&nbsp;&nbsp;
                  <a name='delete' href='". url('delete',['id'=>$value['id']]) ."' title='删除'><i class='fa fa-trash-o text-navy'></i></a>";
              }
          }
      }

      $data = array(
          "draw"=>$draw,
          "recordsTotal"=>$recordsTotal, //数据总数
          "recordsFiltered"=>$recordsFiltered, //过滤之后的记录总数
          "data"=>$lists
      );

      echo json_encode($data);
  }

}
