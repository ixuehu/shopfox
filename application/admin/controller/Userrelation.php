<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use util\Tree;

class Userrelation extends Common
{
    public function index(){
        return view();
    }

    public function team($id = 0){
        $nickname = Db::name('user')->where('id', $id)->value('nickname');

        // 获取当前用户的三级分销团队
        $relation = Db::name('user_relation')->where('pid', $id)->column('uid');
        // 数组转字符串
        $str = implode(',', $relation);
        $user = Db::name('user')->where('id', 'IN', $str)->column('id,pid,nickname');
        $tree = new Tree();
        $tree->tree($user, 'id', 'pid', 'nickname');
        $userList = $tree->getArray($id);

        $this->assign('userList', $userList);
        $this->assign('nickname', $nickname);
        return view();
    }

    public function getDataTables() {
        //获取请求过来的数据
        $getParam = request()->param();

        $draw = $getParam['draw'];

        //排序
        $orderSql = $getParam['order'][0]['dir'];

        //自定义查询参数
        $extra_search = $getParam['extra_search'];

        //获取表名
        $tablename = 'user';
        // 总记录数
        $recordsTotal = Db::name($tablename)->count();
        //过滤条件后的总记录数
        $search = $getParam['search']['value'];
        $recordsFiltered = strlen($search) ?  Db::name($tablename)->where($extra_search,'like','%'.$search.'%')->count() : $recordsTotal;

        //分页
        $start = $getParam['start']; //起始下标
        $length = $getParam['length']; //每页显示记录数

        //根据开始下标计算出当前页
        $page = intval($start/$length) + 1;
        $config = ['page'=>$page, 'list_rows'=>$length];
        $list = Db::name($tablename)->where($extra_search,'like','%'.$search.'%')->order($orderSql)->paginate(null,false,$config);
        $lists = [];
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $lists[$key] = $value;
                $lists[$key]['head'] = "<img src='". $value['head'] ."' width=36>";
                $lists[$key]['operate'] = "<a id='".$value['id']."' href='javascript:;' class='team'>分销团队</a>";
            }
        }

        $data = array(
            "draw"=>$draw,
            "recordsTotal"=>$recordsTotal, //数据总数
            "recordsFiltered"=>$recordsFiltered, //过滤之后的记录总数
            "data"=>$lists
        );

        echo json_encode($data);
    }

}
