<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;

class Distribute extends Common
{
  public function index() {
      return view();
  }

  public function getDataTables() {
      //获取请求过来的数据
      $getParam = request()->param();

      $draw = $getParam['draw'];

      //排序
      $orderSql = $getParam['order'][0]['dir'];

      //自定义查询参数
      $extra_search = $getParam['extra_search'];

      //获取表名
      $tablename = request()->controller();
      // 总记录数
      $recordsTotal = Db::name($tablename)->count();
      //过滤条件后的总记录数
      $search = $getParam['search']['value'];
      $recordsFiltered = strlen($search) ?  Db::name($tablename)->where($extra_search,'like','%'.$search.'%')->count() : $recordsTotal;

      //分页
      $start = $getParam['start']; //起始下标
      $length = $getParam['length']; //每页显示记录数

      //根据开始下标计算出当前页
      $page = intval($start/$length) + 1;
      $config = ['page'=>$page, 'list_rows'=>$length];
      $list = Db::name($tablename)->where($extra_search,'like','%'.$search.'%')->order($orderSql)->paginate(null,false,$config);
      $lists = [];
      if(!empty($list)){
          foreach ($list as $key => $value) {
              $lists[$key] = $value;
              //重设订单状态
              switch ($value['state']) {
                  case 0:
                      $lists[$key]['state'] = '待付款';
                      break;
                  case 1:
                      $lists[$key]['state'] = '已冻结';
                      break;
                  case 2:
                      $lists[$key]['state'] = '解除冻结';
                      break;
                  default:
                      break;
              }
              // 根据order_sn查订单号
              $oid = Db::name('order')->where('order_sn', $value['order_sn'])->value('id');
              $lists[$key]['operate'] = "<a oid='".$oid."' href='javascript:;' class='goods'>订单信息</a>";
          }
      }

      $data = array(
          "draw"=>$draw,
          "recordsTotal"=>$recordsTotal, //数据总数
          "recordsFiltered"=>$recordsFiltered, //过滤之后的记录总数
          "data"=>$lists
      );

      echo json_encode($data);
  }

  //查看订单商品
  public function orderGoods($id = 0){
      $lists = Db::name('order_goods')->where('oid', $id)->select();
      foreach ($lists as $key => $value) {
          $lists[$key]['goods'] = Db::name('goods')->where('id',$value['gid'])->find();
          $lists[$key]['goods']['spec_key_value'] = $value['spec_key_value'];
      }
      $this->assign('lists', $lists);
      return view();
  }
}
