<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use util\Tree;

class Category extends Common
{
    public function index($id = 0, $tab = 1){
        if(request()->isPost()){
            // 获取数组需加/a
            foreach (input('post.sort/a') as $key => $value) {
                Db::name('category')->where('id',$key)->update(['sort'=>$value]);
            }
            return success('排序更新成功',url('index'));
        }else{
            $category =  Db::name('category')->order('sort')->select();
            $tree = new Tree();
            $tree->tree($category, 'id', 'pid', 'name');
            $lists = $tree->getArray();
            $this->assign('lists', $lists);

            //编辑广告
            if(3 == $tab){
                $info = Db::name('category')->where('id', $id)->find();
                if($info){
                    $this->assign('info', $info);
                }
            }

            //规格管理
            if(4 == $tab){
                $spec = Db::name('spec')->where('cid', $id)->order('sort')->select();
                if($spec){
                    $this->assign('spec', $spec);
                }
            }
        }
        return view();
    }

    public function add(){
        if(request()->isPost()){
            $count = Db::name('category')->where('name',input('post.name'))->count();
            if($count){
                return error('分类名称重名');
            }else{
                $result = Db::name('category')->strict(false)->insert(input('post.'));
                if($result){
                    return success('分类添加成功',url('index'));
                }else{
                    return error('分类添加失败');
                }
            }
        }
    }

    public function edit($id = 0){
        if(request()->isPost()){
            $count = Db::name('category')->where('id','<>',$id)->where('name',input('post.name'))->count();
            if($count){
                return error('分类名称重名');
            }else{
                $result = Db::name('category')->strict(false)->update(input('post.'));
                if($result !== false){
                    return success('分类编辑成功',url('index'));
                }else{
                    return error('分类编辑失败');
                }
            }
        }
    }

    public function spec_delete(){
        $id = input('id', 0);
        $cid = input('cid', 0);
        $count = Db::name('spec_item')->where('spec_id', $id)->count();
        if($count){
            return error('此规格下有规格项，无法删除');
        }else{
            if(Db::name('spec')->where('id', $id)->delete()){
                return success('删除成功',url('index',['id'=>$cid,'tab'=>4]));
            }else{
                return error('删除失败');
            }
        }
    }

    public function spec_edit(){
        $data = input('post.');
        // 编辑
        if(count($data['name']) > 0){
            foreach ($data['name'] as $key => $value) {
                if(trim($data['name'][$key]) != ''){
                    Db::name('spec')->where('id', $key)->update(['name'=>trim($data['name'][$key]), 'sort'=>trim($data['sort'][$key])]);
                }
            }
        }
        // 新增
        if(count($data['namenew']) > 0){
            foreach ($data['namenew'] as $key => $value) {
                if(trim($data['namenew'][$key]) != ''){
                    Db::name('spec')->insert(['cid'=>$data['cid'], 'name'=>trim($data['namenew'][$key]), 'sort'=>trim($data['sortnew'][$key])]);
                }
            }
        }
        return success('提交成功',url('index',['id'=>$data['cid'],'tab'=>4]));
    }
}
