<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Cache;

class Index extends Common
{
    public function index(){
        return view();
    }

    public function content(){
        return view();
    }

    // 清除缓存
    public function delRuntime(){
      // 清除模板缓存
      Cache::clear();
      // 清除日志缓存
      //获取日志目录及文件
      $path = glob( LOG_PATH . '/*'); //glob匹配指定的模式返回目录或文件名
      foreach ($path as $item) {
        // 判断文件夹存在
        if(is_dir($item)){
          array_map( 'unlink', glob( $item . '/*.log' )); //array_map执行指定函数
          rmdir($item);
        }
      }
      // 清除temp
      array_map( 'unlink', glob( TEMP_PATH . '/*.php' ));

      $data['status'] = 200;
      $data['msg'] = '缓存删除成功！';
      return json($data);
    }

    public function info(){
        if(request()->isPost()){
            $data = input('post.');
            if($data['password'] == ''){
                return error('密码不为空');
            }else{
                $data['password'] = md5($data['password']);
                $result = db('admin')->where('name',session('user.name'))->update($data);
                if($result !== false){
                    return success('更新成功！');
                }else{
                    return error('更新失败！');
                }
            }
        }else{
            return view();
        }
    }
}
