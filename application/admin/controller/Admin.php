<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;

class Admin extends Common
{
    public function index($id = 0, $tab = 1){
        $lists = Db::name('admin')->select();
        $this->assign('lists', $lists);

        //编辑管理员
        if(3 == $tab){
            $info = Db::name('admin')->where('id', $id)->find();
            if($info){
                $this->assign('info', $info);
            }
        }
        return view();
    }

    public function add(){
        //先判断post提交
        if(request()->isPost()){
            $data = input('post.');
            // 判断名称唯一
            $count = Db::name('admin')->where('name', $data['name'])->count();
            if($count){
              return error('管理员名称已存在！');
            }else{
              $data['password'] = md5($data['password']);
              $newid = Db::name('admin')->strict(false)->insertGetId($data); //strict为false不进行字段严格检查
              if($newid){
                  return success('新管理员添加成功！',url('index'));
              }else{
                  return error('新管理员添加失败！');
              }
            }
        }
    }

    public function edit($id = 0){
        if(request()->isPost()){
            $data = input('post.');
            // 判断密码是否为空，空则不修改密码
            if(trim($data['password']) == ''){
              unset($data['password']);
            }else{
              $data['password'] = md5($data['password']);
            }
            $result = Db::name('admin')->where('id', $id)->strict(false)->update($data);
            //如果信息未改变提交更新会返回false
            if($result !== false){
                return success('管理员更新成功!',url('index'));
            }else{
                return error('管理员更新失败!');
            }
        }
    }

}
