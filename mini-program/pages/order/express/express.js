// pages/order/express/express.js
const util = require('../../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    expressNumber: '',
    expressList: [],
    expressType: '',
    state: 0,
    expressStatusText: ['待揽件', '已揽件', '运输中...', '已签收'],
    imageUrl: '../../../images/holder.png',
    expressChangeList: [],
    wHeight: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var wSize = util.getWindowSize()
    this.setData({ expressNumber: options.expressnum, wHeight: wSize.wHeight })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getExpress(this.data.expressNumber)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 根据快递单号查询快递信息
   */
  getExpress: function (num) {
    var params = { logisticsNo: num }
    util.getExpress100(params, data => {
      var expressList = ''
      if (data.data && data.data.traces && data.data.traces.length > 1) {
        expressList = data.data.traces
      }
      // 当前快递状态
      var state = parseInt(data.data.state)
      // 获取快递公司信息（主要取图标）
      util.getCompany100(params, data => {
        var imageUrl = 'http://7xtwf5.com1.z0.glb.clouddn.com/' + data.data[0].pingyin + '.png'
        this.setData({ imageUrl: imageUrl })
        this.changeData()
      }, data => { }, data => { })

      this.setData({
        expressList: expressList,
        expressType: data.data.logisticCode,
        state: state
      })
    }, data => {
    }, data => {
    })
  },

  changeData: function () {
    var data = this.data.expressList
    var date = ''
    var week = ''
    var time = ''
    var dateTime = ''
    var expressChangeList = []
    const weekArray = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
    for (var i = 0; i < data.length; i++) {
      var length = expressChangeList.length
      week = new Date(data[i].acceptTime).getDay()
      week = weekArray[week]
      dateTime = data[i].acceptTime
      dateTime = dateTime.split(' ') //字符串分割成数组
      date = dateTime[0].split('-')[2] //结果类似11和12
      date = date + '日'
      time = dateTime[1].split(':')
      time = time[0] + ':' + time[1]

      if (expressChangeList.length === 0) {
        expressChangeList[0] = { 'date': date, 'week': week, list: [{ 'time': time, 'acceptTime': data[i].acceptTime, 'acceptStation': data[i].acceptStation }] }
      } else if (expressChangeList.length > 0) {
        if (expressChangeList[length - 1].date === date) {
          expressChangeList[length - 1].list.push({ 'time': time, 'acceptTime': data[i].acceptTime, 'acceptStation': data[i].acceptStation })
        } else {
          expressChangeList[length] = { 'date': date, 'week': week, list: [{ 'time': time, 'acceptTime': data[i].acceptTime, 'acceptStation': data[i].acceptStation }] }
        }
      }
    }
    this.setData({ expressChangeList: expressChangeList })
  }

})