// pages/distribute/index/index.js

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.getUser()
    this.setData({
      userInfo: app.globalData.userInfo
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  /**
   * 提现
   */
  my_tixian: function () {
    wx.navigateTo({
      url: '../tixian/tixian',
    })
  },

  /**
   * 我的团队
   */
  my_team: function () {
    wx.navigateTo({
      url: '../team/team',
    })
  },

  /**
   * 分销订单
   */
  my_bill: function () {
    wx.navigateTo({
      url: '../bill/bill',
    })
  },

  /**
   * 提现明细
   */
  my_money: function () {
    wx.navigateTo({
      url: '../money/money',
    })
  }
})