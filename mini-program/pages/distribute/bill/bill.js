// pages/distribute/bill/bill.js
const util = require('../../../utils/util.js')

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabClasss: [],
    status: ['ALL', 'WAITPAY', 'FROZEN', 'NOFROZEN'],
    stautsDefault: 'ALL',
    page: 1, //默认加载第1页
    orders: [],
    domain: app.globalData.domain
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      tabClasss: ['text-select', 'text-normal', 'text-normal', 'text-normal']
    })
    this.getDistributeList(this.data.stautsDefault, 1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  tabClick: function (e) {
    var index = e.currentTarget.dataset.index
    var status = this.data.status
    var classs = ['text-normal', 'text-normal', 'text-normal', 'text-normal']
    classs[index] = 'text-select'
    this.setData({
      tabClasss: classs,
      stautsDefault: status[index],
      orders: [],
      page: 1
    })
    this.getDistributeList(status[index], 1)
  },

  //默认加载一级团队数据
  getDistributeList: function (status, page) {
    var url = 'User/getDistributeList'
    var params = {
      status: status,
      page: page,
      openid: app.globalData.openid,
      token: app.globalData.userInfo.token
    }
    util.wxRequest(url, params, data => {
      if (data.code == 200) {
        // console.log(data)
        var goods = data.info.data
        var orders = this.data.orders
        for (var i in goods) {
          orders.push(goods[i])
        }
        wx.stopPullDownRefresh()
        this.setData({ orders: orders })
      } else {
        app.globalData.login = false
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    }, data => { }, data => { })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作--刷新
   */
  onPullDownRefresh: function () {
    this.setData({ orders: [], page: 1 })
    this.getDistributeList(this.data.stautsDefault, 1)
  },

  /**
   * 页面上拉触底事件的处理函数--上拉加载
   */
  onReachBottom: function () {
    this.getDistributeList(this.data.stautsDefault, ++this.data.page)
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})