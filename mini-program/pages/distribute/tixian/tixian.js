// pages/distribute/tixian/tixian.js
const util = require('../../../utils/util.js')

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: app.globalData.userInfo
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  tixian: function(e) {
    var money = e.detail.value.money
    var alipay_account = e.detail.value.alipay_account
    var alipay_name = e.detail.value.alipay_name

    // 判断
    if (money == ''){
      wx.showToast({
        title: '提现金额不能为空',
        icon: 'none'
      })
    } else if (alipay_account == ''){
      wx.showToast({
        title: '提现账号不能为空',
        icon: 'none'
      })
    } else if (alipay_name == '') {
      wx.showToast({
        title: '提现账户名不能为空',
        icon: 'none'
      })
    }else{
      if (app.globalData.login){
        var url = 'User/applyMoney'
        var params = {
          money: money,
          alipay_account: alipay_account,
          alipay_name: alipay_name,
          openid: app.globalData.openid,
          token: app.globalData.userInfo.token
        }
        util.wxRequest(url, params, data => {
          if (data.code == 200) {
            wx.showToast({
              title: '提交成功',
              icon: 'success'
            })
            // 可提现佣金减少，已提现佣金增加
            app.globalData.userInfo.money_cash = Number(this.data.userInfo.money_cash - money).toFixed(2)
            // 注意点：微信小程序加法通常是作为连接符，解决方法用减法，如下
            app.globalData.userInfo.money_cashed = Number(this.data.userInfo.money_cashed - (-money)).toFixed(2)
            //返回分销中心
            setTimeout(function(){
              wx.redirectTo({
                url: '../index/index',
              })
            }, 1000)
          } else if(data.code == 400){
            app.globalData.login = false
            wx.showToast({
              title: '请重新登录',
              icon: 'none'
            })
          }else{
            wx.showToast({
              title: data.msg,
              icon: 'none'
            })
          }
        }, data => { }, data => { })
      }else{
        wx.showToast({
          title: '请重新登录',
          icon: 'none'
        })
      }
    }
  }
})