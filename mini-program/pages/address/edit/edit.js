// pages/address/edit/edit.js
const util = require('../../../utils/util.js')

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: 0,
    consignee: '',
    address: '',
    mobile: '',

    //以下为省市区数据相关
    province: [], //取到所有的省
    city: [], //取到所有的市
    area: [], //取到所有的区县
    province_index: 0, //省所在列选择的value值
    city_index: 0, //市所在列选择的value值
    area_index: 0, //区所在列选择的value值
    regions: [], //取到的所有省市区数据
    region: '', //所在地区文本框中的值
    animationData: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ id: options.id})
    this.loadAddress(options.id)

    // 加载省市区数据
    var that = this
    wx.request({
      url: app.globalData.domain + 'regions.json',
      data: {},
      method: 'GET',
      success: function (res) {
        // console.log(res.data)
        that.setData({ regions: res.data })
        that.relation()
      },
      fail: function () { },
      complete: function () { }
    })
  },

  // 地区选择弹出选择框
  chooseRegion: function () {
    // 创建一个动画，显示地区联动
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    // step() 分隔动画
    animation.height(1332 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    })
  },

  // 取消地区选择
  cancelRegion: function () {
    // 创建一个动画，显示地区联动
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    // step() 分隔动画
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export(),
      region: ''
    })
  },

  // 确认地区选择
  confirmRegion: function () {
    // 创建一个动画，显示地区联动
    var animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'ease',
    })
    this.animation = animation
    // step() 分隔动画
    animation.height(0 + 'rpx').step()
    this.setData({
      animationData: animation.export()
    })
  },

  // 处理省市区数据
  relation: function () {
    var that = this
    var regions = that.data.regions
    var province = []
    var city = []
    var area = []
    var province_index = that.data.province_index
    var city_index = that.data.city_index
    var area_index = that.data.area_index
    //遍历所有省，将省名存入province数组
    for (let i = 0; i < regions.length; i++) {
      province.push(regions[i].provinceName)
    }
    // console.log(province)
    if (regions[province_index].cities) {
      if (regions[province_index].cities[city_index]) {
        for (let i = 0; i < regions[province_index].cities.length; i++) {
          city.push(regions[province_index].cities[i].cityName)
        }
        // 以下判断区
        if (regions[province_index].cities[city_index].areas.length) {
          if (regions[province_index].cities[city_index].areas[area_index]) {
            for (let i = 0; i < regions[province_index].cities[city_index].areas.length; i++) {
              area.push(regions[province_index].cities[city_index].areas[i].areaName)
            }
          } else {
            // 此处和市同理
            that.setData({ area_index: 0 })
            for (let i = 0; i < regions[province_index].cities[city_index].areas.length; i++) {
              area.push(regions[province_index].cities[city_index].areas[i].areaName)
            }
          }
        } else {
          // 如果这个市里没有区县，则将市名存入area数组
          area.push(regions[province_index].cities[city_index].cityName)
        }
      } else {
        // 如果选择的省没有下标为city_index的市，则将这个下标设为0，
        // 同时将选中的该省所有市放入city数据
        that.setData({ city_index: 0 })
        for (let i = 0; i < regions[province_index].cities.length; i++) {
          city.push(regions[province_index].cities[i].cityName)
        }
      }
    } else {
      // 如果该省没有市，则将省名作为市和区的名字
      city.push(regions[province_index].provinceName)
      area.push(regions[province_index].provinceName)
    }

    // 最后将数据赋值
    that.setData({
      province: province,
      city: city,
      area: area,
      region: province[that.data.province_index] + city[that.data.city_index] + area[that.data.area_index]
    })
    //如果网络慢，未加载到数据重新获取
    if (province.length == 0 || city.length == 0 || area.length == 0) {
      that.relation()
    }
  },

  // 省市区数据联动
  bindChange: function (e) {
    const val = e.detail.value
    this.setData({
      province_index: val[0],
      city_index: val[1],
      area_index: val[2]
    })
    this.relation()
  },

  nameChange: function (e) {
    this.setData({ consignee: e.detail.value })
  },

  addressChange: function (e) {
    this.setData({ address: e.detail.value })
  },

  mobileChange: function (e) {
    this.setData({ mobile: e.detail.value })
  },

  //加载地址
  loadAddress: function(id) {
    var url = 'User/getAddressById'
    var params = {
      id: id,
      openid: app.globalData.openid,
      token: app.globalData.userInfo.token
    }
    util.wxRequest(url, params, data => {
      if (data.code == 200) {
        var address = data.info
        this.setData({ 
          id: address.id,
          consignee: address.consignee,
          region: address.region,//第三季更新，显示所在地区
          address: address.address,
          mobile: address.mobile,
         })
      } else {
        app.globalData.login = false
        wx.showToast({
          title: '请重新登录',
          icon: 'none'
        })
      }
    }, data => { }, data => { })
  },

  //编辑
  submit: function () {
    var id = this.data.id
    var consignee = this.data.consignee
    var region = this.data.region
    var address = this.data.address
    var mobile = this.data.mobile
    var url = 'User/editAddress'
    var params = {
      id: id,
      consignee: consignee,
      region: region,
      address: address,
      mobile: mobile,
      openid: app.globalData.openid,
      token: app.globalData.userInfo.token
    }
    util.wxRequest(url, params, data => {
      if (data.code == 200) {
        wx.showToast({
          title: '编辑成功',
          icon: 'success'
        })
        //返回收货地址列表页
        wx.navigateBack()
      }else{
        app.globalData.login = false
        wx.showToast({
          title: '请重新登录',
          icon: 'none'
        })
      }
    }, data => { }, data => { })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})