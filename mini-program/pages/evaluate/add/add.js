// pages/evaluate/add/add.js
const util = require('../../../utils/util.js')

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    img: '',
    gid: 0,
    ogid: 0,
    content: '',
    domain: app.globalData.domain
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var name = options.name
    var img = options.img
    var gid = options.gid
    var ogid = options.ogid
    this.setData({
      name: name,
      img: img,
      gid: gid,
      ogid: ogid
    })
  },

  inputing: function(e) {
    var value = e.detail.value
    this.setData({ content: value })
  },

  submit: function (e) {
    var ogid = this.data.ogid
    var gid = this.data.gid
    var content = this.data.content
    var url = 'User/addEvaluate'
    var params = {
      ogid: ogid,
      gid: gid,
      content: content,
      openid: app.globalData.openid,
      token: app.globalData.userInfo.token
    }
    util.wxRequest(url, params, data => {
      if (data.code == 200) {
        wx.showToast({
          title: '感谢您的评价！',
          icon: 'success',
          duration: 2000
        })
        // 返回
        setTimeout(function(){
          wx.navigateBack({
            delta: 1
          })
        }, 2000)
      } else if (data.code == 401){
        app.globalData.login = false
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }else{
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    }, data => { }, data => { })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})