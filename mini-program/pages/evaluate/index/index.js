// pages/evaluate/index/index.js
const util = require('../../../utils/util.js')

//获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabClasss: [],
    status: ['-1', '0', '1'],
    stautsDefault: '-1',
    page: 1, //默认加载第1页
    comments: [],
    domain: app.globalData.domain
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      tabClasss: ['text-select', 'text-normal', 'text-normal']
    })
  },

  //默认加载全部订单数据
  getEvaluateList: function (status, page) {
    var url = 'User/getEvaluateList'
    var params = {
      status: status,
      page: page,
      openid: app.globalData.openid,
      token: app.globalData.userInfo.token
    }
    util.wxRequest(url, params, data => {
      if (data.code == 200) {
        var goods = data.info
        var comments = this.data.comments
        if(goods.length == 0){
          wx.showToast({
            title: '没有更多评价了',
            icon: 'none'
          })
        }else{
          for (var i in goods) {
            comments.push(goods[i])
          }
        }
        // console.log(comments)
        wx.stopPullDownRefresh()
        this.setData({ comments: comments })
      } else {
        app.globalData.login = false
        wx.showToast({
          title: data.msg,
          icon: 'none'
        })
      }
    }, data => { }, data => { })
  },

  tabClick: function (e) {
    var index = e.currentTarget.dataset.index
    var status = this.data.status
    var classs = ['text-normal', 'text-normal', 'text-normal']
    classs[index] = 'text-select'
    this.setData({
      tabClasss: classs,
      stautsDefault: status[index],
      comments: [],
      page: 1
    })
    this.getEvaluateList(status[index], 1)
  },

  addevaluate: function (e) {
    var index = e.currentTarget.dataset.index
    var goods = this.data.comments[index]
    wx.navigateTo({
      url: '../add/add?name=' + goods.name + "&img=" + goods.img + "&gid=" + goods.gid + "&ogid=" + goods.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({ comments: [], page: 1 })
    this.getEvaluateList(this.data.stautsDefault, 1)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({ comments: [], page: 1 })
    this.getEvaluateList(this.data.stautsDefault, 1)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getEvaluateList(this.data.stautsDefault, ++this.data.page)
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})