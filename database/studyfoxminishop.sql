/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : localhost:3306
 Source Schema         : studyfoxminishop

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 25/03/2019 16:18:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fox_ad
-- ----------------------------
DROP TABLE IF EXISTS `fox_ad`;
CREATE TABLE `fox_ad`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '幻灯片0 广告1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `gid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '链接的商品ID',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_ad
-- ----------------------------
INSERT INTO `fox_ad` VALUES (2, 0, '手机', '20180211/062843d860ddd577f192e26b6b62e4c7.jpg', 1, 2);
INSERT INTO `fox_ad` VALUES (3, 0, '日用百货', '20180206/a46d85a0ba172c6b45f2c6f1e2b6b026.jpg', 3, 1);
INSERT INTO `fox_ad` VALUES (4, 1, '电脑', '20180206/18234254764e7d0cf2d85c9592a44b4d.jpg', 2, 3);
INSERT INTO `fox_ad` VALUES (5, 1, 'test4', '20180206/7ca80ccb0240ad13ebdaad74aeaed234.jpg', 4, 4);
INSERT INTO `fox_ad` VALUES (6, 1, 'test5', '20180206/41364f87ff8e4d90336b3f325f4edc91.jpg', 5, 5);
INSERT INTO `fox_ad` VALUES (7, 1, 'test6', '20180206/66792278061b54626a0305ed10cd8092.jpg', 6, 6);
INSERT INTO `fox_ad` VALUES (11, 0, '手机', '20180211/e592cfa8b91486927188319d88ed7344.jpg', 7, 3);

-- ----------------------------
-- Table structure for fox_address
-- ----------------------------
DROP TABLE IF EXISTS `fox_address`;
CREATE TABLE `fox_address`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `consignee` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '收货人',
  `region` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  `mobile` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `is_default` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '默认收货地址',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`uid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_address
-- ----------------------------
INSERT INTO `fox_address` VALUES (1, 1, '雪狐网', '', '上海市XXX', '13361996998', 1);
INSERT INTO `fox_address` VALUES (3, 1, '小雪', '', '上海市杨浦区', '13361996998', 0);
INSERT INTO `fox_address` VALUES (6, 8, '雪狐', '上海市上海市黄浦区', '陆家浜路', '13361996998', 0);
INSERT INTO `fox_address` VALUES (7, 11, '雪狐', '', '上海市', '13361996998', 1);
INSERT INTO `fox_address` VALUES (8, 8, '张三', '上海市上海市静安区', '测试路', '13361996998', 1);

-- ----------------------------
-- Table structure for fox_admin
-- ----------------------------
DROP TABLE IF EXISTS `fox_admin`;
CREATE TABLE `fox_admin`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `password` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_admin
-- ----------------------------
INSERT INTO `fox_admin` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `fox_admin` VALUES (3, 'studyfox', 'e10adc3949ba59abbe56e057f20f883e');

-- ----------------------------
-- Table structure for fox_cart
-- ----------------------------
DROP TABLE IF EXISTS `fox_cart`;
CREATE TABLE `fox_cart`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `gid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `price` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '价格',
  `num` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '购买数量',
  `selected` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '购物车选中状态',
  `spec_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `spec_key_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 46 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_cart
-- ----------------------------
INSERT INTO `fox_cart` VALUES (23, 1, 30, '小米(MI)Air 13.3英寸全金属轻薄笔记本(i5-7200U 8G 256G PCle SSD MX150 2G独显 FHD 指纹识别 ', 0.01, 1, 1, '', '');
INSERT INTO `fox_cart` VALUES (22, 1, 16, '华为手环B3 (蓝牙耳机与智能手环结合+金属机身+触控屏幕+真皮腕带) 商务版 摩卡棕', 1199.00, 1, 0, '', '');
INSERT INTO `fox_cart` VALUES (24, 7, 30, '小米(MI)Air 13.3英寸全金属轻薄笔记本(i5-7200U 8G 256G PCle SSD MX150 2G独显 FHD 指纹识别 ', 0.01, 1, 1, '', '');
INSERT INTO `fox_cart` VALUES (45, 12, 34, '小米8 全面屏游戏智能手机  全网通4G 双卡双待', 0.10, 1, 1, '22_25', '选择颜色:黑色 选择版本:6GB+64GB');

-- ----------------------------
-- Table structure for fox_category
-- ----------------------------
DROP TABLE IF EXISTS `fox_category`;
CREATE TABLE `fox_category`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品分类id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '图片',
  `is_show_index` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '首页显示',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`pid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_category
-- ----------------------------
INSERT INTO `fox_category` VALUES (1, '手机数码', 0, '', 1, 1);
INSERT INTO `fox_category` VALUES (2, '家用电器', 0, '', 1, 2);
INSERT INTO `fox_category` VALUES (3, '电脑办公', 0, '', 1, 3);
INSERT INTO `fox_category` VALUES (4, '家具厨具', 0, '', 0, 4);
INSERT INTO `fox_category` VALUES (5, '服装服饰', 0, '', 0, 5);
INSERT INTO `fox_category` VALUES (6, '美妆个护 ', 0, '', 0, 6);
INSERT INTO `fox_category` VALUES (7, '箱包珠宝', 0, '', 0, 7);
INSERT INTO `fox_category` VALUES (8, '手机', 1, '20180215/f5e3cd84add0985b68b9334b0bca528e.jpg', 0, 1);
INSERT INTO `fox_category` VALUES (9, '手机配件', 1, '20180215/a3d4e138a58a73e47dbdf9075b50156b.jpg', 0, 2);
INSERT INTO `fox_category` VALUES (10, '摄影摄像', 1, '20180215/f3d88f1d515c12fb5ab3b7e4dcc8294a.jpg', 0, 3);
INSERT INTO `fox_category` VALUES (11, '数码配件', 1, '20180215/1f8d285cc658affd7132743eeb68c28b.jpg', 0, 4);
INSERT INTO `fox_category` VALUES (12, '影音娱乐', 1, '20180215/78c3d0479be2c2e158a059bcfaf274c1.jpg', 0, 5);
INSERT INTO `fox_category` VALUES (13, '电子教育', 1, '20180215/463d1cf7d2d8d232bca4bd121a67d88c.jpg', 0, 6);
INSERT INTO `fox_category` VALUES (14, '智能设备', 1, '20180215/941aad6a9a6676cf5b474e18b4731415.jpg', 0, 7);
INSERT INTO `fox_category` VALUES (17, '电视', 2, '20180215/f8a3bc388b5e3d482bf89e15bed4d2b4.jpg', 0, 1);
INSERT INTO `fox_category` VALUES (18, '空调', 2, '20180215/4db552348f4d046d5b1850836b26c976.jpg', 0, 2);
INSERT INTO `fox_category` VALUES (19, '洗衣机', 2, '20180215/204474d43223dc5278aba1119e12089e.jpg', 0, 3);
INSERT INTO `fox_category` VALUES (20, '冰箱', 2, '20180215/910b8a962c69b20f89aa7f02356e10d6.jpg', 0, 4);
INSERT INTO `fox_category` VALUES (21, '电脑', 3, '20180215/2a82d46194946963fcd6e939aa879c51.jpg', 0, 1);
INSERT INTO `fox_category` VALUES (22, '外设', 3, '20180215/d0d387ea162ecc520ff1c699d4673542.jpg', 0, 2);
INSERT INTO `fox_category` VALUES (23, '办公设备', 3, '20180215/2c0092d2d387a7cccf34a732c9e74650.jpg', 0, 3);
INSERT INTO `fox_category` VALUES (24, '家具', 4, '20180215/7b9f0be0b4c69715306d4b6521ee9515.jpg', 0, 1);
INSERT INTO `fox_category` VALUES (25, '厨具', 4, '20180215/84cb93808a5a1dc0cf2a2e45f879c2fd.jpg', 0, 2);
INSERT INTO `fox_category` VALUES (26, '女装', 5, '20180215/5fbb715520574252e310b4cbc019ea9e.jpg', 0, 1);
INSERT INTO `fox_category` VALUES (27, '护肤', 6, '20180215/340404e32739c1c81006b43e51f3a844.jpg', 0, 1);
INSERT INTO `fox_category` VALUES (28, '珠宝首饰', 7, '20180215/f8f1da2dc74391f7a88c6aad2785b222.jpg', 0, 1);

-- ----------------------------
-- Table structure for fox_comment
-- ----------------------------
DROP TABLE IF EXISTS `fox_comment`;
CREATE TABLE `fox_comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ogid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '对应order_goods中的ID',
  `gid` int(10) UNSIGNED NOT NULL,
  `uid` int(10) UNSIGNED NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_comment
-- ----------------------------
INSERT INTO `fox_comment` VALUES (1, 19, 30, 8, '很好的笔记本', '2018-07-07 15:17:28');
INSERT INTO `fox_comment` VALUES (2, 20, 30, 8, '不错啊~', '2018-07-07 15:17:56');
INSERT INTO `fox_comment` VALUES (3, 0, 30, 8, '111', '2018-07-10 19:01:51');
INSERT INTO `fox_comment` VALUES (4, 0, 30, 8, '222', '2018-07-10 19:01:56');
INSERT INTO `fox_comment` VALUES (5, 0, 30, 8, '333', '2018-07-10 19:02:01');
INSERT INTO `fox_comment` VALUES (6, 0, 30, 8, '444', '2018-07-10 19:02:09');
INSERT INTO `fox_comment` VALUES (7, 0, 30, 8, '555', '2018-07-10 19:02:13');
INSERT INTO `fox_comment` VALUES (8, 0, 30, 8, '666', '2018-07-10 19:02:19');
INSERT INTO `fox_comment` VALUES (9, 0, 30, 8, '777', '2018-07-10 19:02:22');
INSERT INTO `fox_comment` VALUES (10, 0, 30, 8, '888', '2018-07-10 19:02:31');
INSERT INTO `fox_comment` VALUES (11, 0, 30, 8, '999', '2018-07-10 19:02:35');
INSERT INTO `fox_comment` VALUES (12, 23, 30, 8, '这是最新评价哈', '2018-07-10 19:03:16');
INSERT INTO `fox_comment` VALUES (13, 24, 30, 8, '7月20日晚评价测试，好，国产精品！', '2018-07-20 21:11:46');
INSERT INTO `fox_comment` VALUES (14, 25, 30, 8, '最新的哈，小米上市了！！', '2018-07-20 21:15:09');

-- ----------------------------
-- Table structure for fox_config
-- ----------------------------
DROP TABLE IF EXISTS `fox_config`;
CREATE TABLE `fox_config`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `info` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_config
-- ----------------------------
INSERT INTO `fox_config` VALUES (1, 'cachetime', '缓存时间', '60');
INSERT INTO `fox_config` VALUES (2, 'watermarkenable', '是否开启图片水印', '0');
INSERT INTO `fox_config` VALUES (3, 'watermarkurl', '图片水印地址', '20180615/daa64672a1f08dd59769bb3ae21fe534.png');
INSERT INTO `fox_config` VALUES (4, 'distribute_open', '是否开启分销功能', '1');
INSERT INTO `fox_config` VALUES (5, 'distribute_level', '分销层级', '3');
INSERT INTO `fox_config` VALUES (6, 'first_rate', '一级分销佣金比例', '20');
INSERT INTO `fox_config` VALUES (7, 'second_rate', '二级分销佣金比例', '10');
INSERT INTO `fox_config` VALUES (8, 'third_rate', '三级分销佣金比例', '5');
INSERT INTO `fox_config` VALUES (9, 'limit', '最低提现额度', '10');

-- ----------------------------
-- Table structure for fox_coupon
-- ----------------------------
DROP TABLE IF EXISTS `fox_coupon`;
CREATE TABLE `fox_coupon`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '优惠券名称',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '优惠券类型(0满减券 1折扣券)',
  `price` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '满减券-减免金额',
  `discount` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '折扣券-折扣率(1-100)',
  `min_price` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '最低消费金额',
  `start_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '优惠券开始时间',
  `end_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '优惠券结束时间',
  `receive_num` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已领取数量',
  `is_delete` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '软删除',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '优惠券记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_coupon
-- ----------------------------
INSERT INTO `fox_coupon` VALUES (1, '满100减10', 0, 10.00, 0, 100.00, 1552406400, 1552406400, 0, 0, 1);
INSERT INTO `fox_coupon` VALUES (2, '满100元9折', 1, 0.00, 90, 100.00, 1552406400, 1552492799, 0, 0, 2);
INSERT INTO `fox_coupon` VALUES (3, '满100减15', 0, 15.00, 0, 100.00, 1553184000, 1553875199, 0, 1, 3);
INSERT INTO `fox_coupon` VALUES (4, '满100减15', 0, 15.00, 0, 100.00, 1553184000, 1553961599, 1, 0, 5);
INSERT INTO `fox_coupon` VALUES (5, '满100减20', 0, 20.00, 0, 100.00, 1553443200, 1554047999, 0, 0, 6);
INSERT INTO `fox_coupon` VALUES (6, '满100元9折', 1, 0.00, 90, 100.00, 1553443200, 1553961599, 0, 0, 7);

-- ----------------------------
-- Table structure for fox_distribute
-- ----------------------------
DROP TABLE IF EXISTS `fox_distribute`;
CREATE TABLE `fox_distribute`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `buyer_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '买家ID',
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '佣金',
  `money_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '获佣时间',
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '佣金状态 0未付款 1已冻结 2解除冻结',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_distribute
-- ----------------------------
INSERT INTO `fox_distribute` VALUES (1, '201811241101444237', 10, 11, 0.20, '2018-11-24 11:01:44', 0);
INSERT INTO `fox_distribute` VALUES (2, '201811241101444237', 9, 11, 0.10, '2018-11-24 11:01:44', 0);
INSERT INTO `fox_distribute` VALUES (3, '201811241101444237', 8, 11, 0.05, '2018-11-24 11:01:44', 0);

-- ----------------------------
-- Table structure for fox_goods
-- ----------------------------
DROP TABLE IF EXISTS `fox_goods`;
CREATE TABLE `fox_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `cid_one` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '一级分类id',
  `cid_two` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '二级分类',
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览量',
  `store` smallint(5) UNSIGNED NOT NULL DEFAULT 10 COMMENT '库存数量',
  `collect` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '收藏数',
  `price` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '价格',
  `weight` decimal(10, 0) UNSIGNED NOT NULL DEFAULT 0 COMMENT '重量(kg)',
  `spec_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '商品规格 单规格1 多规格2',
  `content` text CHARACTER SET utf16 COLLATE utf16_general_ci NULL COMMENT '商品描述',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品图片',
  `sales` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销量',
  `sort` smallint(4) UNSIGNED NOT NULL DEFAULT 50 COMMENT '商品排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cat_id`(`cid_one`) USING BTREE,
  INDEX `goods_number`(`store`) USING BTREE,
  INDEX `sort_order`(`sort`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_goods
-- ----------------------------
INSERT INTO `fox_goods` VALUES (1, 1, 8, 'Apple iPhone X (A1865) 256GB 深空灰色 移动联通电信4G手机', 3, 100, 0, 0.01, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180216/59fbccda158ac65e45a546bd7c1665c2.jpg\" title=\"59fbccda158ac65e45a546bd7c1665c2.jpg\"/></p><p style=\"text-align: center;\"><img src=\"/uploads/20180216/89acca9d8ee93ed93daa955f8f7ed943.jpg\" title=\"89acca9d8ee93ed93daa955f8f7ed943.jpg\"/></p><p style=\"text-align: center;\"><img src=\"/uploads/20180216/56cb4257b44d5b219edcf87ec4a26e42.jpg\" title=\"56cb4257b44d5b219edcf87ec4a26e42.jpg\"/></p><p style=\"text-align: center;\"><img src=\"/uploads/20180216/10ba22e9996c3763257eff988da10d44.jpg\" title=\"10ba22e9996c3763257eff988da10d44.jpg\"/></p><p style=\"text-align: center;\"><img src=\"/uploads/20180216/97772520bf3f834b72b685281491541e.jpg\" title=\"97772520bf3f834b72b685281491541e.jpg\"/></p><p style=\"text-align: center;\"><br/></p>', '20180216/166d6e04fa8ee1e9f0241efc26198011.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (2, 1, 8, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机', 1, 102, 0, 6688.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/b8055c99adafc4ecaa84e36737225875.jpg\" title=\"b8055c99adafc4ecaa84e36737225875.jpg\" alt=\"b8055c99adafc4ecaa84e36737225875.jpg\"/></p>', '20180217/c61841f9bf2be8a7c50c91967b360fc2.jpg', 0, 2);
INSERT INTO `fox_goods` VALUES (3, 1, 8, '华为 HUAWEI Mate 10 Pro 全网通 6GB+128GB 银钻灰 移动联通电信4G手机 双卡双待', 0, 103, 0, 5399.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/58ecf891ee6ae1e4b2db1fe67772771e.jpg\" title=\"58ecf891ee6ae1e4b2db1fe67772771e.jpg\" alt=\"58ecf891ee6ae1e4b2db1fe67772771e.jpg\"/></p>', '20180217/5664870c160d6c2097075aca6b8f9bcc.jpg', 0, 3);
INSERT INTO `fox_goods` VALUES (4, 1, 8, '华为 HUAWEI P10 Plus 全网通4G智能手机 钻雕金 6G+128G', 0, 104, 0, 4488.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/29b10b9ceb62ee8f6d99faf9629a1682.jpg\" title=\"29b10b9ceb62ee8f6d99faf9629a1682.jpg\" alt=\"29b10b9ceb62ee8f6d99faf9629a1682.jpg\"/></p>', '20180217/15af57289c262eef3599171634f78b52.jpg', 0, 4);
INSERT INTO `fox_goods` VALUES (5, 1, 8, '小米MIX2 全网通 6GB 128GB 黑色 移动联通电信4G手机 双卡双待', 0, 105, 0, 3599.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/ff3abb85ccfa5b300f4d114fe2de105f.jpg\" title=\"ff3abb85ccfa5b300f4d114fe2de105f.jpg\" alt=\"ff3abb85ccfa5b300f4d114fe2de105f.jpg\"/></p>', '20180217/17ba99b5a7d32a2b95181eb4ebe11490.jpg', 0, 5);
INSERT INTO `fox_goods` VALUES (6, 1, 8, '小米 Note2 全网通4G 移动联通电信全网通4G智能手机 亮黑色 6+128GB 全球版', 1, 106, 0, 2349.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/ce0485426a5af1f3bf74b91cab5134ee.jpg\" title=\"ce0485426a5af1f3bf74b91cab5134ee.jpg\" alt=\"ce0485426a5af1f3bf74b91cab5134ee.jpg\"/></p>', '20180217/669bbd371cf2bb17c711c18391bc249b.jpg', 0, 6);
INSERT INTO `fox_goods` VALUES (7, 1, 8, '魅族 PRO6S 4GB+64GB 全网通公开版 玫瑰金 移动联通电信4G手机 双卡双待', 1, 107, 0, 1499.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/5cb256563e6caa6e405fcd3d37bdf093.jpg\" title=\"5cb256563e6caa6e405fcd3d37bdf093.jpg\" alt=\"5cb256563e6caa6e405fcd3d37bdf093.jpg\"/></p>', '20180217/643a94466226ff1e9ae50fadd398cd86.jpg', 0, 7);
INSERT INTO `fox_goods` VALUES (8, 1, 8, '三星 Galaxy Note8（SM-N9500）6GB+128GB 谜夜黑 移动联通电信4G手机 双卡双待', 0, 108, 0, 7388.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/542c47bfbc0690a8019075d0fe3c442c.jpg\" title=\"542c47bfbc0690a8019075d0fe3c442c.jpg\" alt=\"542c47bfbc0690a8019075d0fe3c442c.jpg\"/></p>', '20180217/da50f2928784fe1d1b9fcab3a2868a95.jpg', 0, 8);
INSERT INTO `fox_goods` VALUES (9, 1, 8, 'vivo X20 全面屏双摄拍照手机 6GB+64GB 移动联通电信全网通4G手机 双卡双待', 1, 109, 0, 3698.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/e4a71876efd89d2bc785849b1ed7d863.jpg\" title=\"e4a71876efd89d2bc785849b1ed7d863.jpg\" alt=\"e4a71876efd89d2bc785849b1ed7d863.jpg\"/></p>', '20180217/a0e0f1bfbd4d598057849a9e35a006e4.jpg', 0, 9);
INSERT INTO `fox_goods` VALUES (10, 1, 8, '一加手机5T 8GB+128GB 星辰黑 全面屏双摄游戏手机 全网通4G 双卡双待', 1, 110, 0, 3499.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/a14862a6560089e79802b4c5c99cbe0b.jpg\" title=\"a14862a6560089e79802b4c5c99cbe0b.jpg\" alt=\"a14862a6560089e79802b4c5c99cbe0b.jpg\"/></p>', '20180217/13aa8c8912575fccf66f2792d18e5f57.jpg', 0, 10);
INSERT INTO `fox_goods` VALUES (11, 1, 9, '毕亚兹(BIAZE) 苹果6/6S手机壳 iPhone6/6S保护套 全包防摔透明软壳 清爽系列', 0, 110, 0, 8.80, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/8b120732d06659b18f32e811435ccaa6.jpg\" title=\"8b120732d06659b18f32e811435ccaa6.jpg\" alt=\"8b120732d06659b18f32e811435ccaa6.jpg\"/></p>', '20180217/783c94e65e2b8e7364e13c6afdebc73c.jpg', 0, 11);
INSERT INTO `fox_goods` VALUES (12, 1, 10, '索尼（SONY） DSC-RX100M5（RX100V）黑卡数码相机 等效24-70mm F1.8-2.8蔡司镜头', 0, 112, 0, 6149.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/eb58e8811ec4cbc54831e1c7a62d4275.jpg\" title=\"eb58e8811ec4cbc54831e1c7a62d4275.jpg\" alt=\"eb58e8811ec4cbc54831e1c7a62d4275.jpg\"/></p>', '20180217/cf26ced45a4e314fd6b43d24b67e681d.jpg', 0, 12);
INSERT INTO `fox_goods` VALUES (13, 1, 11, '三星（SAMSUNG）存储卡128GB 读速100MB/s 写速90MB/s 4K Class10 高速TF卡（Micro SD卡）红色plus升级版', 2, 116, 0, 245.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/94f02a5ef69fee5496c245481c256c93.jpg\" title=\"94f02a5ef69fee5496c245481c256c93.jpg\" alt=\"94f02a5ef69fee5496c245481c256c93.jpg\"/></p>', '20180217/da9172edb219502bfbcced2b60460a46.jpg', 0, 16);
INSERT INTO `fox_goods` VALUES (14, 1, 12, 'JBL CM102 高保真蓝牙音响 HIFI音质 有源监听音箱 低音炮 多媒体电脑电视音响', 6, 112, 0, 999.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/f7431fc5f0c642abbfc386381267c67a.jpg\" title=\"f7431fc5f0c642abbfc386381267c67a.jpg\" alt=\"f7431fc5f0c642abbfc386381267c67a.jpg\"/></p>', '20180217/61a55e95cd6d66b7dd13f0012c67e830.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (15, 1, 13, '步步高家教机S3 香槟金64G 9.7英寸Retina视网膜屏安全护眼 学生平板电脑学习机 英语点读机点读笔早教机', 4, 123, 0, 3498.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/04d099d2e425372143ab7e8c518466bd.jpg\" title=\"04d099d2e425372143ab7e8c518466bd.jpg\" alt=\"04d099d2e425372143ab7e8c518466bd.jpg\"/></p>', '20180217/20a182d48ccf9efeb09924ec4bdf8191.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (16, 1, 14, '华为手环B3 (蓝牙耳机与智能手环结合+金属机身+触控屏幕+真皮腕带) 商务版 摩卡棕', 57, 169, 0, 1199.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/6c1677741f4380e73c56e3d416c85d41.jpg\" title=\"6c1677741f4380e73c56e3d416c85d41.jpg\" alt=\"6c1677741f4380e73c56e3d416c85d41.jpg\"/></p>', '20180217/ebe473c60aa2371995298b4fd08740f8.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (17, 2, 17, '飞利浦（PHILIPS）65PUF6051/T3 65英寸 二级能效 丰富影视应用 64位4K超高清WIFI智能液晶电视机', 3, 128, 0, 4799.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/90b6e87345f8936fbbfc198bfb4149a6.jpg\" title=\"90b6e87345f8936fbbfc198bfb4149a6.jpg\" alt=\"90b6e87345f8936fbbfc198bfb4149a6.jpg\"/></p>', '20180217/47ea646c035727ea6938678aa8fb06cc.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (18, 2, 18, '格力（GREE）正1.5匹 变频 品圆 冷暖 壁挂式空调 KFR-35GW/(35592)FNhDa-A3', 3, 211, 0, 3399.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/9cacbeda14eda3db62264c260c2942a4.jpg\" title=\"9cacbeda14eda3db62264c260c2942a4.jpg\" alt=\"9cacbeda14eda3db62264c260c2942a4.jpg\"/></p>', '20180217/a6c348f4866c66fa0c742e24a3b7da38.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (19, 2, 19, '西门子 (SIEMENS) XQG80-WM12N1600W 8公斤 变频 滚筒洗衣机 缓震降噪 筒清洁 加漂洗', 6, 100, 0, 3899.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/b6f30a064a24f0f828454b39dedf07dc.jpg\" title=\"b6f30a064a24f0f828454b39dedf07dc.jpg\" alt=\"b6f30a064a24f0f828454b39dedf07dc.jpg\"/></p>', '20180217/1077add1ba6f68fab4d7284f4c834633.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (20, 2, 20, '西门子（SIEMENS）610升 变频风冷无霜 对开门冰箱 LED显示速冷速冻（白色）', 12, 100, 0, 5999.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/15bd1f454de9b715b86abe253a828d80.jpg\" title=\"15bd1f454de9b715b86abe253a828d80.jpg\" alt=\"15bd1f454de9b715b86abe253a828d80.jpg\"/></p>', '20180217/3bc0ebabc6b01ed5083377dc0b4f1e7a.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (21, 3, 21, '戴尔DELL灵越游匣Master15-R4645B 15.6英寸游戏笔记本电脑(i5-7300HQ 8G 128GSSD+1T GTX1050Ti 4G独显)黑', 1, 123, 0, 6399.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180217/e7c018def278df344279948e6f808103.jpg\" title=\"e7c018def278df344279948e6f808103.jpg\" alt=\"e7c018def278df344279948e6f808103.jpg\"/></p>', '20180217/5f1f549e3483239f35825f1d3a96399e.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (22, 3, 22, '酷冷至尊(CoolerMaster) MK750 RGB 幻彩 机械键盘（红轴）樱桃轴（CNC上盖/全键无冲/绝地求生吃鸡键盘）', 1, 121, 0, 1099.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/e5353e3ca4b07c48e856f25f7ab9b52a.jpg\" title=\"e5353e3ca4b07c48e856f25f7ab9b52a.jpg\" alt=\"e5353e3ca4b07c48e856f25f7ab9b52a.jpg\"/></p>', '20180218/a5055dd9efce78d3646ec9c4871d7b4f.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (23, 3, 23, '爱普生（EPSON）L360墨仓式打印机 家用彩色喷墨一体机（打印 复印 扫描）', 12, 120, 0, 999.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/3f0983ada7b3f238b2668f297beb32a2.jpg\" title=\"3f0983ada7b3f238b2668f297beb32a2.jpg\" alt=\"3f0983ada7b3f238b2668f297beb32a2.jpg\"/></p>', '20180218/e4e4426956df9f4e1c910508a4a7ee0c.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (24, 4, 24, '梵爱（VASO LOVE）秒杀直降901 沙发 布艺沙发皮布可拆洗储物沙发客厅家具 ', 0, 121, 0, 3600.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/03f687f4b04602e6fcff553563afeb67.jpg\" title=\"03f687f4b04602e6fcff553563afeb67.jpg\" alt=\"03f687f4b04602e6fcff553563afeb67.jpg\"/></p>', '20180218/230bde6df02385fcd1efac3396525271.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (25, 4, 25, '双立人 ZWILLING 鸿运当头 30cm中式炒锅14件套 40120-035-922', 0, 211, 0, 2288.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/a45f66064d90ca364bce87bb0b3ce300.jpg\" title=\"a45f66064d90ca364bce87bb0b3ce300.jpg\" alt=\"a45f66064d90ca364bce87bb0b3ce300.jpg\"/></p>', '20180218/e1555f01cdfef4589057073713b40b07.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (26, 5, 26, 'intercrew打底衫女套头2018春新款长袖半高领修身保暖女装气质百搭打底弹力针织衫女 ICS1DR53A 黑色', 0, 233, 0, 109.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/18d9dd04302858101b3964bc4af1ecd7.jpg\" title=\"18d9dd04302858101b3964bc4af1ecd7.jpg\" alt=\"18d9dd04302858101b3964bc4af1ecd7.jpg\"/></p>', '20180218/74810328c83ff36916cd2d93d71185d8.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (27, 6, 27, '欧莱雅（LOREAL）男士劲能极润护肤霜 50ml（补水保湿 护肤面霜）', 0, 210, 0, 119.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/1a48d5711ed7845b31e9c8f2d377b53f.jpg\" title=\"1a48d5711ed7845b31e9c8f2d377b53f.jpg\" alt=\"1a48d5711ed7845b31e9c8f2d377b53f.jpg\"/></p>', '20180218/f1c370f07db0c2da32a5d061db359487.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (28, 7, 28, '六福珠宝 网络专款京东JOY联名款足金狗黄金吊坠硬金不含项链', 0, 123, 0, 1690.00, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/a3025197b3c1155aafcdf3cfd4d4acb2.jpg\" title=\"a3025197b3c1155aafcdf3cfd4d4acb2.jpg\" alt=\"a3025197b3c1155aafcdf3cfd4d4acb2.jpg\"/></p>', '20180218/3e9c6e937b9e5c3c98e7681d4c9449c6.jpg', 0, 1);
INSERT INTO `fox_goods` VALUES (30, 3, 21, '小米(MI)Air 13.3英寸全金属轻薄笔记本(i5-7200U 8G 256G PCle SSD MX150 2G独显 FHD 指纹识别 ', 106, 133, 0, 0.01, 0, 1, '<p style=\"text-align: center;\"><img src=\"/uploads/20180218/9f12abe26ec2ed432b29e487b1f315ce.jpg\" title=\"9f12abe26ec2ed432b29e487b1f315ce.jpg\" alt=\"9f12abe26ec2ed432b29e487b1f315ce.jpg\"/></p>', '20180218/964ed0315d3a6063ee8b223c3367f02c.jpg', 0, 2);
INSERT INTO `fox_goods` VALUES (34, 1, 8, '小米8 全面屏游戏智能手机  全网通4G 双卡双待', 85, 100, 0, 0.10, 1, 2, '<p style=\"text-align: center;\"><img src=\"/uploads/20180917/221a46f45072f59070c00876a4afa6da.jpg\" title=\"221a46f45072f59070c00876a4afa6da.jpg\" alt=\"221a46f45072f59070c00876a4afa6da.jpg\"/></p>', '20180917/f9f653c7ed5f86546652a3c427f7d03d.png', 0, 1);

-- ----------------------------
-- Table structure for fox_goods_collect
-- ----------------------------
DROP TABLE IF EXISTS `fox_goods_collect`;
CREATE TABLE `fox_goods_collect`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `gid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`uid`) USING BTREE,
  INDEX `goods_id`(`gid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of fox_goods_collect
-- ----------------------------
INSERT INTO `fox_goods_collect` VALUES (3, 1, 16);
INSERT INTO `fox_goods_collect` VALUES (5, 1, 30);
INSERT INTO `fox_goods_collect` VALUES (11, 8, 34);
INSERT INTO `fox_goods_collect` VALUES (13, 11, 17);
INSERT INTO `fox_goods_collect` VALUES (14, 14, 34);

-- ----------------------------
-- Table structure for fox_goods_images
-- ----------------------------
DROP TABLE IF EXISTS `fox_goods_images`;
CREATE TABLE `fox_goods_images`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品图片id',
  `gid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片地址',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_id`(`gid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 70 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_goods_images
-- ----------------------------
INSERT INTO `fox_goods_images` VALUES (1, 1, '20180217/a906ce80ba7113748dcaebeb24d86b99.jpg');
INSERT INTO `fox_goods_images` VALUES (2, 1, '20180217/fa1eb05bf3a9d27ee323dbe1f598b82c.jpg');
INSERT INTO `fox_goods_images` VALUES (3, 1, '20180217/f78993b92f58849cb9748f494ae71ce3.jpg');
INSERT INTO `fox_goods_images` VALUES (4, 1, '20180217/ef089475c6da9fab11fe5d9c680f584d.jpg');
INSERT INTO `fox_goods_images` VALUES (5, 1, '20180217/065f2c319d14bf245110cc2384151491.jpg');
INSERT INTO `fox_goods_images` VALUES (6, 2, '20180217/60840318790e4c6ac4af94399e12813f.jpg');
INSERT INTO `fox_goods_images` VALUES (7, 2, '20180217/f6c4ee6fcfe7d38fe30afb0aea059260.jpg');
INSERT INTO `fox_goods_images` VALUES (8, 2, '20180217/14b52e6a84a22ad1bf6609c258807bfc.jpg');
INSERT INTO `fox_goods_images` VALUES (9, 2, '20180217/f346deb3343a456f9eae30949a1e497f.jpg');
INSERT INTO `fox_goods_images` VALUES (10, 3, '20180217/540234d2c61de110e41259d4c8c8465b.jpg');
INSERT INTO `fox_goods_images` VALUES (11, 3, '20180217/9a423a0cad615c050f4962720a4e9a31.jpg');
INSERT INTO `fox_goods_images` VALUES (12, 4, '20180217/df6b60744f155d362d580e5439c364cb.jpg');
INSERT INTO `fox_goods_images` VALUES (13, 4, '20180217/91e54e90970eb53a6bf697abf1cdb5e7.jpg');
INSERT INTO `fox_goods_images` VALUES (17, 5, '20180217/89a1c96ce454fa07ff3241fcb9beb399.jpg');
INSERT INTO `fox_goods_images` VALUES (16, 5, '20180217/a7a40d84aba7b41e388c4b89a3aad2cb.jpg');
INSERT INTO `fox_goods_images` VALUES (18, 5, '20180217/266fb41b31379510606a5eccc2e5871d.jpg');
INSERT INTO `fox_goods_images` VALUES (19, 6, '20180217/88ff6e130c0b52b1170b69e9aefb758c.jpg');
INSERT INTO `fox_goods_images` VALUES (20, 6, '20180217/8566ec9a9bfe76bc8dc1214955c97685.jpg');
INSERT INTO `fox_goods_images` VALUES (21, 7, '20180217/73d23701a71819a4abbc00e07bbb7029.jpg');
INSERT INTO `fox_goods_images` VALUES (22, 7, '20180217/fdfa635b5d1540d4555ad4a52e0437a1.jpg');
INSERT INTO `fox_goods_images` VALUES (23, 8, '20180217/c9e27cebe8a04b6f717adb95682cebba.jpg');
INSERT INTO `fox_goods_images` VALUES (24, 8, '20180217/8804bb3f4f1eb2f72b36d8de2c4a2d82.jpg');
INSERT INTO `fox_goods_images` VALUES (25, 9, '20180217/4d468573c34bfe4e0961c6a4b80a2568.jpg');
INSERT INTO `fox_goods_images` VALUES (26, 9, '20180217/5251c023dd3f7ecb24245d43a660adcc.jpg');
INSERT INTO `fox_goods_images` VALUES (27, 10, '20180217/2ec5bae2013570a3962d9fda11828847.jpg');
INSERT INTO `fox_goods_images` VALUES (28, 10, '20180217/d236282d232a0b0fb3d78a5be8c1b423.jpg');
INSERT INTO `fox_goods_images` VALUES (29, 11, '20180217/9343f88d56e60375904215e2f01114a5.jpg');
INSERT INTO `fox_goods_images` VALUES (30, 11, '20180217/4b80aa28c23f6d14d420f6309b4c7510.jpg');
INSERT INTO `fox_goods_images` VALUES (31, 12, '20180217/0237420278173aa1ee98364db8f372cc.jpg');
INSERT INTO `fox_goods_images` VALUES (32, 12, '20180217/05160cd8db0e68c5a031dc153b27b115.jpg');
INSERT INTO `fox_goods_images` VALUES (33, 13, '20180217/de843658fa9974091989949b9ba2ce90.jpg');
INSERT INTO `fox_goods_images` VALUES (34, 13, '20180217/5c9681a62e561c8416d07964032d7e69.jpg');
INSERT INTO `fox_goods_images` VALUES (35, 14, '20180217/e626a24a8971d7d156943dcb92cee3da.jpg');
INSERT INTO `fox_goods_images` VALUES (36, 14, '20180217/c1493d07ce070f0c62cfbcdb64facaa4.jpg');
INSERT INTO `fox_goods_images` VALUES (37, 15, '20180217/b20fe76b42a1577ce82c7d5ba762b0ba.jpg');
INSERT INTO `fox_goods_images` VALUES (38, 15, '20180217/b3a956ff77dca5d3b468dd8d9787c4d8.jpg');
INSERT INTO `fox_goods_images` VALUES (39, 16, '20180217/4a031b248576a9554959fd0fcc01129e.jpg');
INSERT INTO `fox_goods_images` VALUES (40, 16, '20180217/e9fa8d00c2c4a0f18e8d5d5467ad8254.jpg');
INSERT INTO `fox_goods_images` VALUES (41, 17, '20180217/095b9bd21e8ed074988e6f9016e57bd7.jpg');
INSERT INTO `fox_goods_images` VALUES (42, 17, '20180217/db87c33757a4d0888cb11e643a7dcb83.jpg');
INSERT INTO `fox_goods_images` VALUES (43, 18, '20180217/585f9dc73e9c243b4825c969f768c400.jpg');
INSERT INTO `fox_goods_images` VALUES (44, 18, '20180217/3ea4bf49482d5ad2784b5d7ef2bf6186.jpg');
INSERT INTO `fox_goods_images` VALUES (45, 19, '20180217/7e64f881c088442e65abf2ec7b03c4c2.jpg');
INSERT INTO `fox_goods_images` VALUES (46, 19, '20180217/f9ad368ceb740032bb05d881283f8172.jpg');
INSERT INTO `fox_goods_images` VALUES (47, 21, '20180217/b7b304d4533b7d90570e425802a54efd.jpg');
INSERT INTO `fox_goods_images` VALUES (48, 21, '20180217/a523a635fa9c1587d652f57974f3acc2.jpg');
INSERT INTO `fox_goods_images` VALUES (49, 22, '20180218/6b67348b8114a2f4350461d8fd86222b.jpg');
INSERT INTO `fox_goods_images` VALUES (50, 22, '20180218/1f796c7ab4a55e37774195e47b77a147.jpg');
INSERT INTO `fox_goods_images` VALUES (51, 23, '20180218/8db53e0ceb03676751635d1a741b7549.jpg');
INSERT INTO `fox_goods_images` VALUES (52, 23, '20180218/0de238910b014a83b7447801244207cf.jpg');
INSERT INTO `fox_goods_images` VALUES (53, 24, '20180218/d6958ef6c4a32576953d976f8cfdd9c8.jpg');
INSERT INTO `fox_goods_images` VALUES (54, 25, '20180218/b23141a164d5ed13ce63dfa616796ab5.jpg');
INSERT INTO `fox_goods_images` VALUES (55, 25, '20180218/55cac7d1b01c509d43dae42cbdc59d1d.jpg');
INSERT INTO `fox_goods_images` VALUES (56, 26, '20180218/97371cf86a8bb325c237c54d82d523f4.jpg');
INSERT INTO `fox_goods_images` VALUES (57, 26, '20180218/e44db9bded9bb1dd13698003be7c761c.jpg');
INSERT INTO `fox_goods_images` VALUES (58, 27, '20180218/468d8a332b838f0630be760507c5240f.jpg');
INSERT INTO `fox_goods_images` VALUES (59, 27, '20180218/ca794889bc9c68f1846980d84c3677ea.jpg');
INSERT INTO `fox_goods_images` VALUES (60, 28, '20180218/9b82d66bb93eb3d99fa3bd642211659d.jpg');
INSERT INTO `fox_goods_images` VALUES (61, 28, '20180218/48a696a5335182ab648be7a8fc90f88a.jpg');
INSERT INTO `fox_goods_images` VALUES (62, 30, '20180218/930b010d32a8b22e9ab41c1e0d45ce9a.jpg');
INSERT INTO `fox_goods_images` VALUES (63, 30, '20180218/29a16ca312b47787be6a22331bad61e0.jpg');
INSERT INTO `fox_goods_images` VALUES (64, 20, '20180220/19988e81d2c98f5703d1968dd3ae453b.jpg');
INSERT INTO `fox_goods_images` VALUES (65, 20, '20180220/224155427e24b311f89e0e7e364e4fc5.jpg');
INSERT INTO `fox_goods_images` VALUES (66, 34, '20180917/aa2aecb3f2c6b16b21501707774b1683.jpg');
INSERT INTO `fox_goods_images` VALUES (67, 34, '20180917/005a22aee219612cae0f257183f02e6e.jpg');
INSERT INTO `fox_goods_images` VALUES (68, 34, '20180917/b99e22ee70dbecdadf0a20fb95fba3b1.jpg');
INSERT INTO `fox_goods_images` VALUES (69, 34, '20180917/500610bb42bc5e42a332fa8682b588c0.jpg');

-- ----------------------------
-- Table structure for fox_goods_spec
-- ----------------------------
DROP TABLE IF EXISTS `fox_goods_spec`;
CREATE TABLE `fox_goods_spec`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品ID',
  `key` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT '规格ID组合',
  `key_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规格项组合',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '价格',
  `store` int(11) NOT NULL DEFAULT 0 COMMENT '库存',
  `weight` double UNSIGNED NOT NULL DEFAULT 0 COMMENT '重量(kg)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_goods_spec
-- ----------------------------
INSERT INTO `fox_goods_spec` VALUES (22, 34, '22_25', '选择颜色:黑色 选择版本:6GB+64GB', 0.10, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (23, 34, '23_25', '选择颜色:白色 选择版本:6GB+64GB', 0.20, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (24, 34, '24_25', '选择颜色:金色 选择版本:6GB+64GB', 0.30, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (25, 34, '25_27', '选择版本:6GB+64GB 选择颜色:红色', 0.70, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (26, 34, '22_26', '选择颜色:黑色 选择版本:6GB+128GB', 0.40, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (27, 34, '23_26', '选择颜色:白色 选择版本:6GB+128GB', 0.50, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (28, 34, '24_26', '选择颜色:金色 选择版本:6GB+128GB', 0.60, 100, 1);
INSERT INTO `fox_goods_spec` VALUES (29, 34, '26_27', '选择版本:6GB+128GB 选择颜色:红色', 0.80, 100, 1);

-- ----------------------------
-- Table structure for fox_money_apply
-- ----------------------------
DROP TABLE IF EXISTS `fox_money_apply`;
CREATE TABLE `fox_money_apply`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '提现金额',
  `alipay_account` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' COMMENT '支付宝账号',
  `alipay_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支付宝姓名',
  `apply_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '申请时间',
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '审核状态 0审核中 1已打款',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_money_apply
-- ----------------------------
INSERT INTO `fox_money_apply` VALUES (1, 8, 10.00, 'test@qq.com', '', '2018-12-14 14:38:14', 0);
INSERT INTO `fox_money_apply` VALUES (2, 8, 10.00, 'test@qq.com', '张三', '2018-12-14 14:39:22', 0);
INSERT INTO `fox_money_apply` VALUES (3, 8, 11.00, 'abc@qq.com', '王五', '2018-12-14 14:56:47', 0);
INSERT INTO `fox_money_apply` VALUES (4, 8, 14.00, 'test@qq.com', '李四', '2018-12-14 14:58:29', 1);
INSERT INTO `fox_money_apply` VALUES (5, 8, 10.00, 'test@qq.com', '张三', '2018-12-14 15:20:45', 1);
INSERT INTO `fox_money_apply` VALUES (6, 8, 10.00, 'test@qq.com', '张三', '2018-12-14 15:54:38', 1);

-- ----------------------------
-- Table structure for fox_order
-- ----------------------------
DROP TABLE IF EXISTS `fox_order`;
CREATE TABLE `fox_order`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_sn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '订单号，小程序中显示用',
  `order_sn_submit` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '订单号，微信支付时提交用，可变',
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `order_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单状态 待付款0 已完成1 待发货2 待收货3 已取消4',
  `expressnum` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物流单号',
  `consignee` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '收货人',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机',
  `amount` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '订单总价',
  `submit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `order_sn`(`order_sn`) USING BTREE,
  INDEX `user_id`(`uid`) USING BTREE,
  INDEX `order_status`(`order_status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_order
-- ----------------------------
INSERT INTO `fox_order` VALUES (1, '201802282306596079', '201802282306595449', 1, 0, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-02-28 23:06:59');
INSERT INTO `fox_order` VALUES (2, '201803011009004336', '201803011009003677', 1, 1, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:09:00');
INSERT INTO `fox_order` VALUES (3, '201803011017093931', '201803011017096974', 1, 2, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:17:09');
INSERT INTO `fox_order` VALUES (4, '201803011038211642', '201803011038212936', 1, 3, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:38:21');
INSERT INTO `fox_order` VALUES (5, '201803011039367958', '201803011039364973', 1, 4, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:39:36');
INSERT INTO `fox_order` VALUES (6, '201803011041393069', '201803011041394920', 1, 0, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:41:39');
INSERT INTO `fox_order` VALUES (7, '201803011043254627', '201803011043254714', 1, 0, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:43:25');
INSERT INTO `fox_order` VALUES (8, '201803011045061392', '201803011045063053', 1, 0, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:45:06');
INSERT INTO `fox_order` VALUES (9, '201803011048024437', '201803011048029671', 1, 1, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 10:48:02');
INSERT INTO `fox_order` VALUES (10, '201803011159294275', '201803011159292647', 1, 3, '285572776440', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 11:59:29');
INSERT INTO `fox_order` VALUES (11, '201803011225118268', '201803011225111689', 1, 1, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 12:25:11');
INSERT INTO `fox_order` VALUES (12, '201803011229346701', '201803011229344402', 1, 4, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 12:29:34');
INSERT INTO `fox_order` VALUES (13, '201803011306437426', '201803011306438421', 1, 0, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 13:06:43');
INSERT INTO `fox_order` VALUES (14, '201803011316562925', '201803011316562586', 1, 0, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 13:16:56');
INSERT INTO `fox_order` VALUES (15, '201803011328207948', '201803021056266204', 1, 3, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 13:28:20');
INSERT INTO `fox_order` VALUES (16, '201803011331588013', '201803011331582799', 1, 4, '', '雪狐网', '上海市XXX', '13361996998', 0.01, '2018-03-01 13:31:58');
INSERT INTO `fox_order` VALUES (18, '201806070940117699', '201806070940114081', 8, 1, '', '雪狐', '上海市', '13361996998', 2098.12, '2018-06-07 09:40:11');
INSERT INTO `fox_order` VALUES (19, '201806070940372327', '201806070940374580', 8, 1, '', '雪狐', '上海市', '13361996998', 0.01, '2018-06-07 09:40:37');
INSERT INTO `fox_order` VALUES (20, '201806070950416203', '201806070950415975', 8, 2, '', '雪狐', '上海市', '13361996998', 3899.00, '2018-06-07 09:50:41');
INSERT INTO `fox_order` VALUES (21, '201807202104428743', '201807202104425394', 8, 3, '285572776440', '雪狐', '上海市', '13361996998', 0.02, '2018-07-20 21:04:42');
INSERT INTO `fox_order` VALUES (22, '201807202114309093', '201807202114305047', 8, 3, '285572776440', '雪狐', '上海市', '13361996998', 0.03, '2018-07-20 21:14:30');
INSERT INTO `fox_order` VALUES (23, '201810101715343178', '201810101715349252', 8, 3, '285572776440', '雪狐', '上海市', '13361996998', 0.60, '2018-10-10 17:15:34');
INSERT INTO `fox_order` VALUES (24, '201811202118514179', '201811202118518175', 11, 0, '', '雪狐', '上海市', '13361996998', 0.10, '2018-11-20 21:18:51');
INSERT INTO `fox_order` VALUES (25, '201811241101444237', '201811241101443788', 11, 0, '', '雪狐', '上海市', '13361996998', 1.00, '2018-11-24 11:01:44');

-- ----------------------------
-- Table structure for fox_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `fox_order_goods`;
CREATE TABLE `fox_order_goods`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `oid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单id',
  `gid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id',
  `num` smallint(5) UNSIGNED NOT NULL DEFAULT 1 COMMENT '购买数量',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '价格',
  `spec_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `spec_key_value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `comment_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否评价 否0 是1',
  `submit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`oid`) USING BTREE,
  INDEX `goods_id`(`gid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_order_goods
-- ----------------------------
INSERT INTO `fox_order_goods` VALUES (1, 1, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (2, 2, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (3, 3, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (4, 4, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (5, 5, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (6, 6, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (7, 7, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (8, 8, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (9, 9, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (10, 10, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (11, 11, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (12, 12, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (13, 13, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (14, 14, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (15, 15, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (16, 16, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (19, 18, 30, 12, 0.01, '', '', 1, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (20, 18, 22, 1, 1099.00, '', '', 1, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (21, 18, 23, 1, 999.00, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (22, 19, 30, 1, 0.01, '', '', 0, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (23, 20, 19, 1, 3899.00, '', '', 1, '2018-07-15 19:24:33');
INSERT INTO `fox_order_goods` VALUES (24, 21, 30, 2, 0.01, '', '', 1, '2018-07-20 21:04:42');
INSERT INTO `fox_order_goods` VALUES (25, 22, 30, 3, 0.01, '', '', 1, '2018-07-20 21:14:30');
INSERT INTO `fox_order_goods` VALUES (26, 23, 34, 1, 0.60, '24_26', '选择颜色:金色 选择版本:6GB+128GB', 0, '2018-10-10 17:15:34');
INSERT INTO `fox_order_goods` VALUES (27, 24, 34, 1, 0.10, '22_25', '选择颜色:黑色 选择版本:6GB+64GB', 0, '2018-11-20 21:18:51');
INSERT INTO `fox_order_goods` VALUES (28, 25, 34, 10, 0.10, '22_25', '选择颜色:黑色 选择版本:6GB+64GB', 0, '2018-11-24 11:01:44');

-- ----------------------------
-- Table structure for fox_spec
-- ----------------------------
DROP TABLE IF EXISTS `fox_spec`;
CREATE TABLE `fox_spec`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类ID',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规格名',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_spec
-- ----------------------------
INSERT INTO `fox_spec` VALUES (5, 8, '选择颜色', 1);
INSERT INTO `fox_spec` VALUES (6, 8, '选择版本', 2);

-- ----------------------------
-- Table structure for fox_spec_item
-- ----------------------------
DROP TABLE IF EXISTS `fox_spec_item`;
CREATE TABLE `fox_spec_item`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spec_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '规格ID',
  `item` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规格项',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of fox_spec_item
-- ----------------------------
INSERT INTO `fox_spec_item` VALUES (22, 5, '黑色');
INSERT INTO `fox_spec_item` VALUES (23, 5, '白色');
INSERT INTO `fox_spec_item` VALUES (24, 5, '金色');
INSERT INTO `fox_spec_item` VALUES (25, 6, '6GB+64GB');
INSERT INTO `fox_spec_item` VALUES (26, 6, '6GB+128GB');
INSERT INTO `fox_spec_item` VALUES (27, 5, '红色');

-- ----------------------------
-- Table structure for fox_user
-- ----------------------------
DROP TABLE IF EXISTS `fox_user`;
CREATE TABLE `fox_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父ID',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `head` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像 从小程序获取',
  `money` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '用户金额',
  `total_amount` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '消费累计',
  `reg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '微信验证后返回openid',
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `token_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `money_frozen` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '冻结佣金',
  `money_cash` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '可提现佣金',
  `money_cashed` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '已提现佣金',
  `first_member` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '一级会员',
  `second_member` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '二级会员',
  `third_member` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '三级会员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fox_user
-- ----------------------------
INSERT INTO `fox_user` VALUES (8, 0, '含笑01', 'https://wx.qlogo.cn/mmopen/vi_32/oamP7I0SbBSUdnyReF2K1kQEFkXZc4A4nUJeNLG7x979Oo72eyseYZ9PoWRgJee8SnO7Kib1Rw6bBU2x68JeTXg/132', 0.00, 0.00, '2018-05-26 00:04:35', 'oJeb54tmKq7BDSsnt41_GgEjRf9g_01', 'zpyGlo3kkvU3S6dsP7caXTu8DgfaH1B2', 1550565516, 560.11, 260.69, 88.88, 1, 1, 1);
INSERT INTO `fox_user` VALUES (9, 8, '雪狐', 'https://wx.qlogo.cn/mmopen/vi_32/ZSJzXA9VMrr4q8s5bJ4Q7uma0l7CFF6XhsHFsy9ibVXG1h4R0uCONs9Ltk9ttODAABSUMnAOcXDue5yEsfLNpog/132', 0.00, 0.00, '2018-10-09 19:37:36', 'oJeb54j8hDcNPkgGigg8FSBf4Qs4', '96QySapfxZWL0pxOJWbNFp46pvZYPahR', 1539085155, 0.00, 0.00, 0.00, 1, 1, 0);
INSERT INTO `fox_user` VALUES (10, 9, '小张', 'https://wx.qlogo.cn/mmopen/vi_32/ZSJzXA9VMrr4q8s5bJ4Q7uma0l7CFF6XhsHFsy9ibVXG1h4R0uCONs9Ltk9ttODAABSUMnAOcXDue5yEsfLNpog/132', 0.00, 0.00, '2018-11-14 14:59:16', '7Jeb54j8hDcNPkgGigg8FSBf4Qs5', '96QySapfxZWL0pxOJWbNFp46pvZYPahR', 1539085155, 0.00, 0.00, 0.00, 1, 0, 0);
INSERT INTO `fox_user` VALUES (11, 10, '小江', 'https://wx.qlogo.cn/mmopen/vi_32/oamP7I0SbBSUdnyReF2K1kQEFkXZc4A4nUJeNLG7x979Oo72eyseYZ9PoWRgJee8NtibX3O54sdicIJUW71qwLFw/132', 0.00, 0.00, '2018-11-14 15:00:18', '91eb54tmKq7BDSsnt41_GgEjRf9g', 'RhUmBLbXBZSB9RuvWZOWzDafNQ5i83EZ', 1543653706, 1230.68, 500.88, 23.52, 0, 0, 0);
INSERT INTO `fox_user` VALUES (14, 0, '含笑', 'https://wx.qlogo.cn/mmopen/vi_32/oamP7I0SbBSUdnyReF2K1kQEFkXZc4A4nUJeNLG7x979Oo72eyseYZ9PoWRgJee8NtibX3O54sdicIJUW71qwLFw/132', 0.00, 0.00, '2019-03-02 16:13:38', 'oJeb54tmKq7BDSsnt41_GgEjRf9g', '0UDhf9TaE3oMNEK33DvDS5R8AS8rcsLc', 1553394229, 0.00, 0.00, 0.00, 0, 0, 0);

-- ----------------------------
-- Table structure for fox_user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `fox_user_coupon`;
CREATE TABLE `fox_user_coupon`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `coupon_id` smallint(11) UNSIGNED NOT NULL DEFAULT 0,
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for fox_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `fox_user_relation`;
CREATE TABLE `fox_user_relation`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of fox_user_relation
-- ----------------------------
INSERT INTO `fox_user_relation` VALUES (1, 8, 1, 9);
INSERT INTO `fox_user_relation` VALUES (2, 8, 2, 10);
INSERT INTO `fox_user_relation` VALUES (3, 8, 3, 11);
INSERT INTO `fox_user_relation` VALUES (4, 9, 1, 10);
INSERT INTO `fox_user_relation` VALUES (5, 9, 2, 11);
INSERT INTO `fox_user_relation` VALUES (6, 10, 1, 11);

SET FOREIGN_KEY_CHECKS = 1;
