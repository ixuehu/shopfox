# ShopFox 免费开源电商系统 [永久更新源码和开发视频]
### （全程录制手把手开发教程，让学习和二次开发更容易）


雪狐专注国内领先的B2C电商系统；<br>
遵循Apache2开源协议发布，并提供免费使用；<br>
微信小程序商城系统（配备全程开发教程），后期将增加PC版，微信商城等多版本。


#### 安装教程
1. http://www.studyfox.cn/262.html<br>
2. http://www.studyfox.cn/259.html<br>


## 付费说明
代码开源，录制课程需要投入大量的时间和精力，如需学习全程开发视频，请访问雪狐网哦~<br>
http://www.studyfox.cn/258.html<br>
http://www.studyfox.cn/260.html


#### 官网及QQ群
官网：http://www.studyfox.cn<br>
QQ群：775586


<img src="http://www.studyfox.cn/Uploads/video/2018-11/201811021848209775.jpg" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-12/201812191130026385.jpg" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-12/201812191130139375.jpg" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-12/201812191130234472.jpg" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-11/201811021848325338.png" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-11/201811021848418096.png" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-12/201812191128449973.jpg" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-12/201812191129004738.jpg" alt="" /><br>
<img src="http://www.studyfox.cn/Uploads/video/2018-12/201812191129412475.jpg" alt="" /><br>


#### 开源协议
Apache License Version 2.0 see http://www.apache.org/licenses/LICENSE-2.0.html
